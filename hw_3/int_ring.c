/* Code for MPI ring communication - processor zero starts with message = 0 and passes it onto processor 1, 
each processor adds its rank to the message before sending it. Last processor sends it back to processor 0. 
This happens N times */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <mpi.h>
#include "util.h"
#define array

int main(int argc, char *argv[])
{
   int rank,tag,origin,destination,comm_size,N,n;

   MPI_Init(&argc,&argv);
   MPI_Comm_rank(MPI_COMM_WORLD,&rank);
   MPI_Comm_size(MPI_COMM_WORLD,&comm_size);
   int Num_ints = 0;
#ifdef array
   Num_ints = 250000;
#else
   Num_ints = 1; 
#endif

   // int Num_ints = 1; 

   if (argc != 2)
   {
      fprintf(stderr,"Needs number of rounds of communication as input! \n");
      MPI_Abort(MPI_COMM_WORLD,1);
   }

   sscanf(argv[1], "%d", &N);
   MPI_Status status;

   char hostname[1024];
   gethostname(hostname,1024);
  
   //int message = 0;
   int *message = calloc(Num_ints,sizeof(int));

   for (n=0; n < Num_ints; n++) { message[n] = 0;} 

   tag = 0;
   
   int act_message = 0; 
   int i,j=1; 

    if (rank == 0) 
    {
       for (j=1; j < comm_size; j ++) { act_message += j; } 
       act_message = N*act_message; 
    }


   /* Timing */
   timestamp_type time1, time2;
   get_timestamp(&time1);
   
   for (i=1; i <= N; i++)
   {

      if (rank != 0) 
      {
         destination = rank; 
         origin = rank-1;
         MPI_Recv(message,Num_ints,MPI_INT,origin,tag,MPI_COMM_WORLD,&status);  
         printf("rank %d hosted on %s  has recieved message from rank %d. Message is: %d \n",rank,hostname,rank-1,message[0]);
      }
   
      origin = rank; 
      destination = (rank+1)%comm_size; // Account for the last processor to ensure it goes to processor 0
      for (n = 0; n < Num_ints; n++){ message[n] += rank;}
      // message += rank;
      MPI_Send(message,Num_ints,MPI_INT,destination,tag,MPI_COMM_WORLD);

      if (rank == 0)
      {
         destination = rank; 
         origin = comm_size-1;
         MPI_Recv(message,Num_ints,MPI_INT,comm_size-1,tag,MPI_COMM_WORLD,&status);
         printf("rank %d hosted on %s  has recieved message from rank %d. Message is: %d \n",rank,hostname,origin,message[0]);
      }
   }

   get_timestamp(&time2);
   double elapsed = timestamp_diff_in_seconds(time1,time2);

   if (rank == 0)
   { 
      printf("Message should be: %d. Message is: %d \n \n ",act_message,message[0]);
      printf("Time elapsed is %f seconds.\n", elapsed);
   }
   free(message);
   MPI_Finalize(); 
   return 0; 
}
